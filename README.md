# Peake ILG Middleware

## Installation Instructions
1. Install Node.js 10+ (latest LTS) using detailed installation instructions [here](https://nodejs.org/en/download/package-manager/)
2. Clone repository
    ```
    $ git clone https://bitbucket.org/folio3/peake123-ilg.git
    ```
3. Change into the working directory
    ```
    $ cd middleware
    ```
4. Execute following commands to install NPM dependencies
    ```
    $ npm install
    ```
## Start Application
Run start command to start the application entry point
```
$ npm start
```
## Build Application
Run build command to create dist dorectroy in middleware root
```
$ npm run build
```
## On Production Server
1. Install pm2 (first time only)
    ```
    $ npm install -g pm2
    ```
2. Clone repository
    ```
    $ git clone https://bitbucket.org/folio3/peake123-ilg.git
    ```
3. Change into the working directory
    ```
    $ cd middleware
    ```
4. Execute following commands to install NPM dependencies
    ```
    $ npm install
    ```    
5. Run build command to create dist dorectroy in middleware root
    ```
    $ npm run build
    ```

## PM2 Commands
1. Start Application using config file
    ```
    $ pm2 start ecosystem.config.js --env <env_name>
    ```
2. Restart Application
    ```
    $ pm2 restart <APP_NAME> --env <env_name>
    ```
3. Stop Application
    ```
    $ pm2 stop <APP_NAME>
    ```
4. Stop All
    ```
    $ pm2 stop all
    ```