module.exports = {
  apps: [{
    name: 'Peake-Middleware-Sandbox-App',
    script: './dist/app.js',
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    // uid: 'root',
    // gid: 'ilg-sftp-sandbox',
    env: {
      NODE_ENV: 'development',
    },
    env_sandbox: {
      NODE_ENV: 'sandbox',
    },
    env_production: {
      NODE_ENV: 'production',
    },
  }],
};
