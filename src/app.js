import * as cron from 'node-cron';
import SalesOrder from './modules/SalesOrder';
import CreditMemo from './modules/CreditMemo';
import PurchaseOrder from './modules/PurchaseOrder';
import Items from './modules/Items';
import ItemReceipts from './modules/ItemReceipts';
import PurchaseOrderReceipt from './modules/PurchaseOrderReceipt';
import SalesOrderDispatch from './modules/SalesOrderDispatch';
import SalesOrderStatus from './modules/SalesOrderStatus';
import InventoryAdjustment from './modules/InventoryAdjustment';
import InventoryTransfer from './modules/InventoryTransfer';
import ReturnReceipt from './modules/ReturnReceipt';
import TransferOrder from './modules/TransferOrder';
import TransferOrderConfirmationStatus from './modules/TransferOrderConfirmationStatus';
import WorkOrder from './modules/WorkOrders';
import WorkOrderIssue from './modules/WorkOrderIssue';


console.log('Application Started...');

// /**
//  * SalesOrder Sync
//  */
// cron.schedule('*/1 * * * *', () => {
//   console.log('SalesOrder sync started');
//   SalesOrder().catch(({ name, message }) => console.error('SalesOrder Error:', { name, message }));
//   console.log('SalesOrder sync end');
// });

/**
 * CreditMemo Sync
 */
// cron.schedule('*/1 * * * *', () => {
//   console.log('CreditMemo sync started');
//   CreditMemo().catch(({ name, message }) => console.error('CreditMemo Error:', { name, message }));
//   console.log('CreditMemo sync end');
// });

// /**
//  * PurchaseOrder Sync
//  */
// cron.schedule('*/2 * * * *', () => {
//   console.log('PurchaseOrder sync started');
//   PurchaseOrder().catch(({ name, message }) => console.error('PurchaseOrder Error:', { name, message }));
//   console.log('PurchaseOrder sync end');
// });

// /**
//  * Items Sync
//  */
// cron.schedule('*/3 * * * *', () => {
//   console.log('Items sync started');
//   Items().catch(({ name, message }) => console.error('Items Error:', { name, message }));
//   console.log('Items sync end');
// });

/**
 * Item Receipts Sync
 */

cron.schedule('*/1 * * * *', () => {
  console.log('Item Receipts sync started');
  ItemReceipts().catch(({ name, message }) => console.error('Items Error:', { name, message }));
  console.log('Items sync end');
});


/**
 * Workorder Sync
 */

cron.schedule('*/2 * * * *', () => {
  console.log('Work Order sync started');
  WorkOrder().catch(({ name, message }) => console.error('Items Error:', { name, message }));
  console.log('Work order sync end');
});

/**
 * WorkorderIssue Sync
 */

cron.schedule('*/3 * * * *', () => {
  console.log('Work Order Issue sync started');
  WorkOrderIssue().catch(({ name, message }) => console.error('Items Error:', { name, message }));
  console.log('Work order Issue sync end');
});


// /**
//  * PurchaseOrderReceipt Sync
//  */
// cron.schedule('*/4 * * * *', () => {
//   console.log('PurchaseOrderReceipt sync started');
//   PurchaseOrderReceipt().catch(({ name, message }) => console.error('PurchaseOrderReceipt Error:', { name, message }));
//   console.log('PurchaseOrderReceipt sync end');
// });

// /**
//  * SalesOrderDispatch Sync
//  */
// cron.schedule('*/5 * * * *', () => {
//   console.log('SalesOrderDispatch sync started');
//   SalesOrderDispatch().catch(({ name, message }) => console.error('SalesOrderDispatch Error:', { name, message }));
//   console.log('SalesOrderDispatch sync end');
// });

// /**
//  * SalesOrderStatus Sync
//  */
// cron.schedule('*/6 * * * *', () => {
//   console.log('SalesOrderStatus sync started');
//   SalesOrderStatus().catch(({ name, message }) => console.error('SalesOrderStatus Error:', { name, message }));
//   console.log('SalesOrderStatus sync end');
// });

// /**
//  * InventoryAdjustment Sync
//  */
// cron.schedule('*/8 * * * *', () => {
//   console.log('InventoryAdjustment sync started');
//   InventoryAdjustment().catch(({ name, message }) => console.error('InventoryAdjustment Error:', { name, message }));
//   console.log('InventoryAdjustment sync end');
// });

// /**
//  * InventoryTransfer Sync
//  */
// cron.schedule('*/9 * * * *', () => {
//   console.log('InventoryTransfer sync started');
//   InventoryTransfer().catch(({ name, message }) => console.error('InventoryTransfer Error:', { name, message }));
//   console.log('InventoryTransfer sync end');
// });

// /**
//  * ReturnReceipt Sync
//  */
// cron.schedule('*/7 * * * *', () => {
//   console.log('ReturnReceipt sync started');
//   ReturnReceipt().catch(({ name, message }) => console.error('ReturnReceipt Error:', { name, message }));
//   console.log('ReturnReceipt sync end');
// });

// /**
//  * TransferOrder Sync
//  */
// cron.schedule('*/10 * * * *', () => {
//   console.log('TransferOrder sync started');
//   TransferOrder().catch(({ name, message }) => console.error('TransferOrder Error:', { name, message }));
//   console.log('TransferOrder sync end');
// });

// /**
//  * TransferOrderConfirmationStatus Sync
//  */

// cron.schedule('*/11 * * * *', () => {
//   console.log('TransferOrderConfirmationStatus sync started');
//   TransferOrderConfirmationStatus().catch(({ name, message }) => console.error('TransferOrderConfirmationStatus Error:', { name, message }));
//   console.log('TransferOrderConfirmationStatus sync end');
// });

console.log('Application is running...');
