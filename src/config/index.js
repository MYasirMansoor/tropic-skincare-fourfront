import development from './development';
import sandbox from './sandbox';
import production from './production';

const { NODE_ENV = 'development' } = process.env; // 'sandbox' or 'production'

const CONFIG = { development, sandbox, production };

export default CONFIG[NODE_ENV];
