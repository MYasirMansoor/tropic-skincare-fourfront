import ImportCsv from '../../utils/ImportCsv';
import COMMON from '../../config/common';
import AsyncLock from '../../utils/AsyncLockHandler';

const {
  ROUTES: { IMPORT_CREDIT_MEMO },
  IMPORT_TYPE: { CREDIT_MEMO },
  GLOBAL_CONSTANTS: { THRESHOLD_LIMIT: { MAX_LINES } },
} = COMMON;

const lock = new AsyncLock(true);

const getData = async () => {
  try {
    await lock.acquire();

    const recordType = CREDIT_MEMO;
    const action = IMPORT_CREDIT_MEMO;

    await ImportCsv({
      recordType,
      action,
      processBy: '',
      key: '',
      chunkLines: MAX_LINES,
    });
  } catch (error) {
    throw error;
  } finally {
    lock.release();
  }
};

export default getData;
