import groupBy from 'lodash/groupBy';
import ImportCsv from '../../utils/ImportCsv';
import COMMON from '../../config/common';
import AsyncLock from '../../utils/AsyncLockHandler';

const {
  ROUTES: { IMPORT_INVENTORY_ADJUSTMENT },
  IMPORT_TYPE: { INVENTORY_ADJUSTMENT },
  GLOBAL_CONSTANTS: { THRESHOLD_LIMIT: { MAX_LINES } },
} = COMMON;

const lock = new AsyncLock(true);

const getData = async () => {
  try {
    await lock.acquire();

    const recordType = INVENTORY_ADJUSTMENT;
    const action = IMPORT_INVENTORY_ADJUSTMENT;

    await ImportCsv({
      recordType,
      action,
      processBy: groupBy,
      key: 'ILG_Reference',
      chunkLines: MAX_LINES,
    });
  } catch (error) {
    throw error;
  } finally {
    lock.release();
  }
};

export default getData;
