import groupBy from 'lodash/groupBy';
import ImportCsv from '../../utils/ImportCsv';
import COMMON from '../../config/common';
import AsyncLock from '../../utils/AsyncLockHandler';

const {
  ROUTES: { IMPORT_PURCHASE_ORDER_RECEIPT },
  IMPORT_TYPE: { ITEM_RECEIPT },
  GLOBAL_CONSTANTS: { THRESHOLD_LIMIT: { MAX_LINES } },
} = COMMON;

const lock = new AsyncLock(true);

const getData = async () => {
  try {
    await lock.acquire();

    const recordType = ITEM_RECEIPT;
    const action = IMPORT_PURCHASE_ORDER_RECEIPT;

    await ImportCsv({
      recordType,
      action,
      processBy: groupBy,
      key: 'PO Number',
      chunkLines: MAX_LINES,
    });
  } catch (error) {
    throw error;
  } finally {
    lock.release();
  }
};

export default getData;
