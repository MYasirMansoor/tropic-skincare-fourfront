import groupBy from 'lodash/groupBy';
import ImportCsv from '../../utils/ImportCsv';
import COMMON from '../../config/common';
import AsyncLock from '../../utils/AsyncLockHandler';

const {
  ROUTES: { IMPORT_RETURN_RECEIPT },
  IMPORT_TYPE: { RETURN_RECEIPT },
  GLOBAL_CONSTANTS: { THRESHOLD_LIMIT: { MAX_LINES } },
} = COMMON;

const lock = new AsyncLock(true);

const getData = async () => {
  try {
    await lock.acquire();

    const recordType = RETURN_RECEIPT;
    const action = IMPORT_RETURN_RECEIPT;

    await ImportCsv({
      recordType,
      action,
      processBy: groupBy,
      key: 'Return_Reference',
      chunkLines: MAX_LINES,
    });
  } catch (error) {
    throw error;
  } finally {
    lock.release();
  }
};

export default getData;
