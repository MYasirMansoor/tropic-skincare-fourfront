import _ from 'lodash';
import globalConstants from '../../config/globalConstants';


const {
  FF_CSV_MAP,
  CUSTOMER,
  SALES_ORDER,
} = globalConstants;

const calculateProRatedValues = (data) => {
  const sObj = [];
  _.forEach(data, (result) => {
    // Keep Prorated Values Empty For Header and Payment Lines
    if (result.indicator === FF_CSV_MAP.HEADER || isPaymentItem(result.indicator)) {
      _.assign(result, { custcol_f3_prorated_amt: '', custcol_f3_prorated_vat: '' });
      sObj.push(result);
      return;
    }
    if (!isDiscountItem(result.description)) {
      // Prorated Values for Inventory Item
      const proRatedVat = calculateProRatedVat(result.tax1amt, result.quantity);
      const proRatedAmount = '';
      _.assign(result, { custcol_f3_prorated_amt: proRatedAmount, custcol_f3_prorated_vat: proRatedVat });
    } else if (isDiscountItem(result.description)) {
      // Prorated Values for Discount Items
      const item = _.find(data, { custcol_f3_unique_line_id: result.custcol_f3_item_identifier });
      const proRatedVat = calculateProRatedVat(result.tax1amt, item.quantity);
      const proRatedAmount = calculateProRatedAmount(result.rate, item.quantity);
      _.assign(result, { custcol_f3_prorated_amt: proRatedAmount, custcol_f3_prorated_vat: proRatedVat });
    }
    sObj.push(result);
  });
  return sObj;
};


const isPaymentItem = indicator => !!indicator && indicator.toLowerCase().indexOf('pmt') !== -1;


/**
 *
 * @param {*} vatAmt
 * @param {*} qty
 */
const calculateProRatedVat = (vatAmt, qty) => (parseFloat(vatAmt) / parseInt(qty)).toFixed(10).toString();

/**
 *
 * @param {*} amount
 * @param {*} qty
 */
const calculateProRatedAmount = (amount, qty) => (parseFloat(amount) / parseInt(qty)).toFixed(10).toString();


/**
 *
 * @param {*} data
 * @returns {array} sales order object
 */
const mapData = (data) => {
  const sObj = [];
  let payment = 0;
  let previousItem = '';
  data.forEach((result) => {
    if (result[FF_CSV_MAP.INDICATOR_INDEX] === FF_CSV_MAP.HEADER) {
      // Map Header (Body Fields)
      sObj.push(mapHeader(result));
    } else if (result[FF_CSV_MAP.INDICATOR_INDEX] === FF_CSV_MAP.LINE) {
      // Map Line Fields

      // Caching Item for referencing in Discount Items
      if (!isDiscountItem(result[FF_CSV_MAP.DESC_INDEX]) && !isDeliveryItem(result[FF_CSV_MAP.DESC_INDEX])) {
        previousItem = result[FF_CSV_MAP.LINE_ID_INDEX];
      }

      const mappedObj = mapLine(result);

      // Adding Reference of Item in Discount Item
      if (isDiscountItem(result[FF_CSV_MAP.DESC_INDEX])) {
        _.assign(mappedObj, { custcol_f3_item_identifier: previousItem });
      }
      sObj.push(mappedObj);
    } else if (result[FF_CSV_MAP.INDICATOR_INDEX] === FF_CSV_MAP.PAYMENT) {
      // Map Payment Lines
      payment += mapPayment(result);
    }
  });
  sObj.push({ payment, indicator: 'PMT' });
  // console.log(sObj);
  return sObj;
};

/**
 *
 * @param {*} data
 * @returns {Array} sales order objects
 */
const removeComponents = (data) => {
  const sObj = [];
  const kits = [];
  data.forEach((record) => {
    if (record[FF_CSV_MAP.KIT_INDICATOR] === 'KIT') {
      kits.push(record[FF_CSV_MAP.CSV_ITEM]);
    }
    if (_.indexOf(kits, record[FF_CSV_MAP.KIT_INDICATOR])) {
      sObj.push(record);
    }
  });
  return sObj;
};

/**
 *
 * @param {*} description
 */
const isDiscountItem = description => !!description && description.toLowerCase().indexOf('discount') !== -1;

/**
 *
 * @param {*} description
 */
const isDeliveryItem = description => !!description && description.toLowerCase().indexOf('delivery') !== -1;


/**
 *
 * @param {*} data
 */
const insertRate = (data) => {
  const sOrderArray = [];
  _.forEach(data, (record) => {
    if (record[FF_CSV_MAP.INDICATOR_INDEX] === FF_CSV_MAP.LINE) {
      if (isDiscountItem(record[FF_CSV_MAP.DESC_INDEX]) || isDeliveryItem(record[FF_CSV_MAP.DESC_INDEX])) {
        // Rate Calculation for Discount and Delivery item
        record.push(((record[FF_CSV_MAP.DI_AMT] - record[FF_CSV_MAP.VAT_INDEX]) / record[FF_CSV_MAP.QUANTITY_INDEX]).toFixed(10).toString());
      } else {
        // Rate Calculation for Inventory Item
        record.push(((record[FF_CSV_MAP.AMT_INDEX] / record[FF_CSV_MAP.QUANTITY_INDEX]).toFixed(10)).toString());
      }
    }
    sOrderArray.push(record);
  });
  return sOrderArray;
};

const mapPayment = (result) => {
  const obj = {};
  for (const key in SALES_ORDER.PAYMENT_MAP) {
    obj[key] = result[SALES_ORDER.PAYMENT_MAP[key]];
  }
  return parseFloat(obj.payment);
};

/**
 *
 * @param {*} result
 * @returns {object} mapped object as key/value pair to NS fields
 */
const mapHeader = (result) => {
  const obj = {};
  obj.indicator = FF_CSV_MAP.HEADER;
  for (const key in SALES_ORDER.HEADER_MAP) {
    obj[key] = result[SALES_ORDER.HEADER_MAP[key]];
  }
  return obj;
};

/**
 *
 * @param {*} result
 * @returns {object} mapped object as key/value pair to NS fields
 */
const mapLine = (result) => {
  const obj = {};
  for (const key in SALES_ORDER.LINE_MAP) {
    if (key === 'price') {
      obj[key] = '-1';
      continue;
    }
    if (key === 'taxcode') {
      obj[key] = '9';
      continue;
    }
    obj[key] = result[SALES_ORDER.LINE_MAP[key]];
  }
  return obj;
};


/**
 *
 * @param {*} data
 * @returns {array} sales order object grouped
 */
const groupSO = (data) => {
  const obj = [];
  let index = -1;

  data.forEach((sObj) => {
    if (sObj.indicator === FF_CSV_MAP.HEADER) {
      index += 1;
      obj[index] = [];
    }
    obj[index].push(sObj);
  });

  return obj;
};


/**
 *
 * @param {*} data
 * @returns {array} sales order object grouped
 */
const customerData = (data) => {
  const custArray = [];

  // Map Customer Data
  data.forEach((sObj) => {
    const bodyObj = {};
    const addObj = {};
    bodyObj[CUSTOMER.AUTO_NAME] = false;
    bodyObj[CUSTOMER.IS_PERSON] = 'T';
    if (sObj[0] === FF_CSV_MAP.HEADER) {
      for (const key in CUSTOMER.CUSTOMER_MAP) {
        if (key === CUSTOMER.NAME) {
          // Split Up the name of customer into First, Middle and Last Name
          const [first, middle, ...last] = sObj[CUSTOMER.CUSTOMER_MAP[key]].split(' ');
          bodyObj[CUSTOMER.FIRST_NAME] = first;
          bodyObj[CUSTOMER.MIDDLE_NAME] = middle;
          bodyObj[CUSTOMER.LAST_NAME] = _.join(last, ' ');
          continue;
        }
        if (key === FF_CSV_MAP.EMAIL) {
          // Appending donot in email during testing
          bodyObj[key] = `donot_${sObj[CUSTOMER.CUSTOMER_MAP[key]]}`;
          continue;
        }
        bodyObj[key] = sObj[CUSTOMER.CUSTOMER_MAP[key]];
      }
      for (const key in CUSTOMER.CUSTOMER_ADDRESS_MAP) {
        // Mapping Address Fields For Customer
        addObj[key] = sObj[CUSTOMER.CUSTOMER_ADDRESS_MAP[key]];
      }
      custArray.push({ bodyObj, addObj });
    }
  });
  return custArray;
};


const parseSalesOrder = (dataToImport) => {
  const dataCopy = [];

  _.assign(dataCopy, dataToImport);
  // Mapping Raw data according to netsuite

  // Remove Kit items components
  dataToImport = removeComponents(dataToImport);

  // Calculate Discount Items Total and Items Net Value total (Total of Items, Total Of discount) and Remove Discount Items here

  // Place Net value in separate Column and Calculate Discount based net value here (Net Value Of Item + (Net Value of Item/Total of Items * Total Of Discount))

  // Calculate VAT % through reverse mapping

  // Insert VAT through percentage


  // Calculate Rate from Amount
  dataToImport = insertRate(dataToImport);

  // Map Index Based JSON data to Netsuite Key Value Pair
  dataToImport = mapData(dataToImport);

  // Calculate Prorated VAT and Rate per quantity
  // Add Prorated Value Calculation for Discount also
  dataToImport = calculateProRatedValues(dataToImport);

  // Group data by Sales Orders
  dataToImport = groupSO(dataToImport);

  // Creating and Mapping Customer Object in case of customer not created in netsuite.
  const mappedCustomerData = customerData(dataCopy);

  return { dataToImport, mappedCustomerData };
};

export default parseSalesOrder;
