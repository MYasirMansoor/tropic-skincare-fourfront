import ImportCsv from '../../utils/ImportCsv';
import COMMON from '../../config/common';
import AsyncLock from '../../utils/AsyncLockHandler';

const {
  ROUTES: { GET_SALES_ORDER },
  IMPORT_TYPE: { SALES_ORDER },
  GLOBAL_CONSTANTS: { THRESHOLD_LIMIT: { MAX_LINES } },
} = COMMON;

const lock = new AsyncLock(true);

const getData = async () => {
  try {
    await lock.acquire();
    const recordType = SALES_ORDER;
    const action = GET_SALES_ORDER;
    await ImportCsv({
      recordType,
      action,
      processBy: '',
      key: '',
      chunkLines: MAX_LINES,
    });
  } catch (error) {
    console.error(error);
  } finally {
    lock.release();
  }
};

export default getData;
