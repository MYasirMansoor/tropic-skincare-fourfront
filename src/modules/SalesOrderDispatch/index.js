import groupBy from 'lodash/groupBy';
import ImportCsv from '../../utils/ImportCsv';
import COMMON from '../../config/common';
import AsyncLock from '../../utils/AsyncLockHandler';

const {
  ROUTES: { IMPORT_SALES_ORDER_DISPATCH },
  IMPORT_TYPE: { ITEM_FULFILLMENT },
  GLOBAL_CONSTANTS: { THRESHOLD_LIMIT: { MAX_LINES } },
} = COMMON;

const lock = new AsyncLock(true);

const getData = async () => {
  try {
    await lock.acquire();

    const recordType = ITEM_FULFILLMENT;
    const action = IMPORT_SALES_ORDER_DISPATCH;

    await ImportCsv({
      recordType,
      action,
      processBy: groupBy,
      key: 'Client_Ref',
      chunkLines: MAX_LINES,
    });
  } catch (error) {
    throw error;
  } finally {
    lock.release();
  }
};

export default getData;
