import keyBy from 'lodash/keyBy';
import ImportCsv from '../../utils/ImportCsv';
import COMMON from '../../config/common';
import AsyncLock from '../../utils/AsyncLockHandler';


const {
  ROUTES: { IMPORT_SALES_ORDER_STATUS },
  IMPORT_TYPE: { SALES_ORDER_STATUS },
  GLOBAL_CONSTANTS: { THRESHOLD_LIMIT: { MAX_LINES } },
} = COMMON;

const lock = new AsyncLock(true);

const getData = async () => {
  try {
    lock.acquire();

    const recordType = SALES_ORDER_STATUS;
    const action = IMPORT_SALES_ORDER_STATUS;

    await ImportCsv({
      recordType,
      action,
      processBy: keyBy,
      key: 'CT_Ref',
      chunkLines: MAX_LINES,
    });
  } catch (error) {
    throw error;
  } finally {
    lock.release();
  }
};

export default getData;
