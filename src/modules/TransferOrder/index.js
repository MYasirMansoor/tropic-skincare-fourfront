import postRequest from '../../utils/Http';
import ExportCsv from '../../utils/ExportCsv';
import COMMON from '../../config/common';
import AsyncLock from '../../utils/AsyncLockHandler';

const { ROUTES: { GET_TRANSFER_ORDER }, EXPORT_TYPE: { TRANSFER_ORDER } } = COMMON;

const lock = new AsyncLock(true);

const getData = async () => {
  try {
    await lock.acquire();
    const response = await postRequest({ action: GET_TRANSFER_ORDER });
    if (response && response.data) {
      console.log('postRequest response', response.data);
      if (!response.data.errorCode) {
        ExportCsv(response.data, TRANSFER_ORDER);
      }
    }
  } catch (error) {
    console.error(error);
  } finally {
    lock.release();
  }
};

export default getData;
