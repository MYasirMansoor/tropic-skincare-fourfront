import keyBy from 'lodash/keyBy';
import ImportCsv from '../../utils/ImportCsv';
import COMMON from '../../config/common';
import AsyncLock from '../../utils/AsyncLockHandler';


const {
  ROUTES: { IMPORT_TRANSFER_ORDER_CONFIRMATION_STATUS },
  IMPORT_TYPE: { TRANSFER_ORDER_CONFIRMATION },
  GLOBAL_CONSTANTS: { THRESHOLD_LIMIT: { MAX_LINES } },
} = COMMON;

const lock = new AsyncLock(true);

const getData = async () => {
  try {
    lock.acquire();

    const recordType = TRANSFER_ORDER_CONFIRMATION;
    const action = IMPORT_TRANSFER_ORDER_CONFIRMATION_STATUS;

    await ImportCsv({
      recordType,
      action,
      processBy: keyBy,
      key: 'Customer_Ref',
      chunkLines: MAX_LINES,
    });
  } catch (error) {
    throw error;
  } finally {
    lock.release();
  }
};

export default getData;
