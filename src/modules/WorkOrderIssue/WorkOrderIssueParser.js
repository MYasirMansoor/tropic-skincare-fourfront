import _ from 'lodash';
import globalConstants from '../../config/globalConstants';

const {
  WORK_ORDER_ISSUE,
} = globalConstants;

const mapRecord = (data) => {
  const woiObj = {};
  _.forEach(WORK_ORDER_ISSUE, (index, fieldName) => {
    woiObj[fieldName] = data[index];
  });
  return woiObj;
};

const parseWorkOrderIssue = (dataToImport) => {
  dataToImport = mapRecord(dataToImport[0]);
  return [dataToImport];
};

export default parseWorkOrderIssue;
