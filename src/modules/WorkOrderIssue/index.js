import ImportCsv from '../../utils/ImportCsv';
import COMMON from '../../config/common';
import AsyncLock from '../../utils/AsyncLockHandler';

const {
  ROUTES: { GET_WORK_ORDER_ISSUE },
  IMPORT_TYPE: { WORK_ORDER_ISSUE },
  GLOBAL_CONSTANTS: { THRESHOLD_LIMIT: { MAX_LINES } },
} = COMMON;

const lock = new AsyncLock(true);

const getData = async () => {
  try {
    await lock.acquire();
    const recordType = WORK_ORDER_ISSUE;
    const action = GET_WORK_ORDER_ISSUE;
    await ImportCsv({
      recordType,
      action,
      processBy: '',
      key: '',
      chunkLines: MAX_LINES,
    });
  } catch (error) {
    console.error(error);
  } finally {
    lock.release();
  }
};

export default getData;
