import postRequest from '../../utils/Http';
import ExportCsv from '../../utils/ExportCsv';
import COMMON from '../../config/common';
import AsyncLock from '../../utils/AsyncLockHandler';

const { ROUTES: { GET_WORK_ORDER }, EXPORT_TYPE: { WORK_ORDER } } = COMMON;
const lock = new AsyncLock(true);

const getData = async () => {
  try {
    await lock.acquire();
    const response = await postRequest({ action: GET_WORK_ORDER });
    // console.log('response', response);
    if (response && response.data) {
    //   console.log('postRequest response', response.data);
      if (!response.data.errorCode) {
        ExportCsv(response.data, WORK_ORDER);
      }
    }
  } catch (error) {
    console.error(error);
  } finally {
    lock.release();
  }
};

export default getData;
