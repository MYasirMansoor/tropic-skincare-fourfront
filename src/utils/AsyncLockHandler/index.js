export default class AwaitLock {
  constructor(strictMode = false) {
    this._acquired = false;
    this._waitingResolvers = [];
    this._strictMode = strictMode;
  }

  acquire() {
    if (!this._acquired) {
      this._acquired = true;
      return Promise.resolve();
    }

    return new Promise((resolve, reject) => {
      if (this._strictMode) {
        reject();
      }
      this._waitingResolvers.push(resolve);
    });
  }

  hasResolvers() {
    return this._waitingResolvers.length > 0;
  }

  release() {
    if (this.hasResolvers()) {
      const resolve = this._waitingResolvers.shift();
      resolve();
    } else {
      this._acquired = false;
    }
  }
}
