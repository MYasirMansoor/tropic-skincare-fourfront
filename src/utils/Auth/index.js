import crypto from 'crypto';
import OAuth from 'oauth-1.0a';
import CONFIG from '../../config';

const {
  CONSUMER, TOKEN, REALM, ENDPOINT,
} = CONFIG;

const getAuthorizationHeader = (requestData) => {
  const oauth = OAuth({
    consumer: { key: CONSUMER.key, secret: CONSUMER.secret },
    signature_method: 'HMAC-SHA1',
    realm: REALM,
    hash_function(baseString, key) {
      return crypto.createHmac('sha1', key).update(baseString).digest('base64');
    },
  });

  return oauth.toHeader(oauth.authorize(requestData, TOKEN));
};

export default {
  getAuthorizationHeader,
  ENDPOINT,
};
