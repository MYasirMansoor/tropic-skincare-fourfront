import csv from 'csv';
import fs from 'fs';
import _ from 'lodash';
// import postRequest from '../Http';
import CONFIG from '../../config';


const { SFTP: { EXPORT_PATH, FILE_NAME } } = CONFIG;

const createCsv = (data, recordType) => {
  console.log('Export start for', recordType);
  const directory = EXPORT_PATH[recordType];
  const filename = FILE_NAME[recordType];


  if (_.isEmpty(data)) {
    console.log('Data not available for', recordType);
    return false;
  }

  let i = 0;
  for (const key in data) {
    // eslint-disable-next-line no-prototype-builtins
    if (data.hasOwnProperty(key)) {
      // Added i here because the timestamp was repeating on middleware server
      const csvFileName = `${directory}/${filename}${Date.now() + i}.csv`;

      const obj = data[key];
      const stringifier = csv.stringify(obj, { header: false });

      const writable = fs.createWriteStream(csvFileName);
      i += 1;
      stringifier.pipe(writable);

      stringifier.on('finish', () => {
        console.log('File Created ', csvFileName);
      });

      stringifier.on('error', (error) => {
        const errorMessage = error.message || 'error!!!';
        console.error(errorMessage, filename);
        //   postRequest({
        //     action: 'CatchError', message: 'Can not create CSV file', messageDetails: errorMessage, recordType: filename, recordId: null, syncType: filename, syncFlow: 'EXPORT', data,
        //   });
        stringifier.emit('end');
      });
      // stringifier.end();
    }
  }
};

export default createCsv;
