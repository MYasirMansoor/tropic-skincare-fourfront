import fs from 'fs-extra';
import path from 'path';
import csv from 'csv';

/**
 * Move file from source path to destination path
 * @param {*} sourcePath
 * @param {*} destinationPath
 */
export async function move(sourcePath, destinationPath) {
  try {
    await fs.move(sourcePath, destinationPath, { overwrite: true });
  } catch (error) {
    throw error;
  }
}

/**
 * Remove file
 * @param {*} sourcePath
 */
export async function remove(sourcePath) {
  try {
    await fs.remove(sourcePath);
  } catch (error) {
    throw error;
  }
}

/**
 * Check file
 * @param {*} sourcePath
 */
export function isFile(sourcePath) {
  try {
    return fs.lstatSync(sourcePath).isFile();
  } catch (error) {
    throw error;
  }
}

/**
 * Read directory
 * @param {*} sourcePath
 */
export async function readDir(sourcePath) {
  try {
    return await fs.readdir(sourcePath);
  } catch (error) {
    throw error;
  }
}

/**
 * Generate filename from path
 * @param {*} directoryPath
 * @param {*} fileName
 */
export function generateFileName(directoryPath, fileName) {
  try {
    return path.join(directoryPath, fileName);
  } catch (error) {
    throw error;
  }
}

/**
 * List All files in directory
 * @param {*} directoryPath
 */
export async function listFiles(directoryPath) {
  try {
    const dir = await readDir(directoryPath);
    return dir.map(fileName => generateFileName(directoryPath, fileName)).filter(isFile) || [];
  } catch (error) {
    throw error;
  }
}

/**
 * Read Stream
 * @param {*} fileName
 */
export async function readFileStream(fileName) {
  try {
    return await fs.createReadStream(fileName);
  } catch (error) {
    throw error;
  }
}

/**
 * Write stream
 * @param {*} fileName
 */
export async function createFileStream(fileName) {
  try {
    return await fs.createWriteStream(fileName);
  } catch (error) {
    throw error;
  }
}

/**
 * CSV stream promise
 * @param {*} stream
 */
function csvStreamPromise(stream) {
  return new Promise((resolve, reject) => {
    stream.on('end', () => {
      resolve(true);
    });

    stream.on('finish', () => {
      resolve('true');
    });

    stream.on('error', (error) => {
      reject(error);
      console.log('stream error');
    });
  });
}

/**
 * Create CSV file
 * @param {*} data
 * @param {*} fileName
 */
export async function createCSV(data, fileName) {
  try {
    const stringifier = csv.stringify(data, {
      header: true,
    });

    const writable = await createFileStream(fileName);
    stringifier.pipe(writable);

    return await csvStreamPromise(stringifier);
  } catch (error) {
    throw error;
  }
}

/**
 * Read csv file
 * @param {*} fileName
 */
export async function readCSV(fileName) {
  try {
    const csvData = [];
    const readable = await readFileStream(fileName);

    const csvParse = csv.parse({
      columns: false,
    });

    readable.pipe(csvParse);

    csvParse.on('data', (data) => {
      csvData.push(data);
    });

    return await csvStreamPromise(csvParse) && csvData;
  } catch (error) {
    throw error;
  }
}

/**
 * Create Multiple files from Chunk data array
 * @param {*} chunkData
 * @param {*} fileName
 */
export async function createChunkCSV(chunkData, fileName) {
  try {
    const chunkFilename = `${fileName.replace(/\.[^/.]+$/, '')}_chunk`;
    const files = [];
    for (const [index, data] of chunkData.entries()) {
      const file = `${chunkFilename}${index}.csv`;
      try {
        await createCSV(data, file);
        files.push(file);
      } catch (error) {
        console.error(file, error);
      }
    }
    return files;
  } catch (error) {
    throw error;
  }
}

/**
 * Write CSV file
 * @param {object} data
 * @param {string} fileName
 */
export async function writeCSV(data, fileName) {
  try {
    const writable = await createFileStream(fileName);

    const stringifier = csv.stringify(data, {
      header: true,
    });

    stringifier.pipe(writable);

    return await csvStreamPromise(stringifier) && fileName;
  } catch (error) {
    throw error;
  }
}

/**
 * Get File name from base path
 * @param {string} fileName
 */
export function getFileName(fileName) {
  try {
    return path.basename(fileName);
  } catch (error) {
    throw error;
  }
}
