'use strict';

/**
 *@NApiVersion 2.x
 *@NScriptType Suitelet
 *@NModuleScope Public
 */

import ui from 'N/ui/serverWidget';
import runtime from 'N/runtime';
import https from 'N/https';

const onRequest = ({request, response}) => {
  const currentScript = runtime.getCurrentScript();
  const params = request.parameters;
  const method = request.method;
  try {
    const scriptIds = params['custpage_script_ids'] || '';

    let scriptIdsExist = !(scriptIds === '');
    const scriptIdsParsed = [];

    if (scriptIdsExist) {
      const scriptIdsArr = scriptIds.split(',');

      for (let index = 0; index < scriptIdsArr.length; index += 1) {
        const scriptId = (scriptIdsArr[index] + '').trim();
        if (scriptId.indexOf('customscript') === 0 &&
          scriptIdsParsed.indexOf(scriptId) === -1) {
          scriptIdsParsed.push(scriptId);
        }
      }

      if (scriptIdsParsed.length === 0) {
        scriptIdsExist = false;
      }
    }

    if (method === 'GET') {
      let submitButtonLabel;
      const form = ui.createForm({title: 'Generate Guid Form'});

      if (scriptIdsExist) {
        const field = form.addSecretKeyField({
          id: 'custpage_secret_key',
          label: 'Secret Key',
          restrictToScriptIds: scriptIdsParsed,
          restrictToCurrentUser: false,
        }).maxLength = 64;
        field.defaultValue = 'ZFNbooKXMZefh97tr29N';

        const helpField = form.addField({
          label: 'Key Help',
          type: ui.FieldType.INLINEHTML,
          id: 'custpage_key_help',
        });
        helpField.defaultValue = '' +
          '<div>' +
          '   <div>' +
          '     <b>Important: </b>The generated GUID can be consumed by the scripts  having following Ids:' +
          '   </div>' +
          '   <br/><br/>' +
          '   <div>' +
          '     <b>' + scriptIds + '</b>' +
          '   </div>' +
          '</div>' +
          '   <br/><br/>' +
          '<div>' +
          '   <span>' +
          '     <b>Note: </b> Please prefix the secret key with AWS4 for Amazon e.g. the value entered here should be like "AWS4wxzKAtPK..."' +
          '   </span>' +
          '</div>' +
          '';

        submitButtonLabel = 'Generate Secret GUID';
      } else {
        const scriptIdsFld = form.addField({
          id: 'custpage_script_ids',
          label: 'Script Ids (Comma separated)',
          type: ui.FieldType.LONGTEXT,
        });
        scriptIdsFld.defaultValue = currentScript.id;
        scriptIdsFld.isMandatory = true;

        submitButtonLabel = 'Submit for generating GUID';
      }

      form.addSubmitButton({label: submitButtonLabel});
      response.writePage(form);

    } else {
      const generatedGUID = params['custpage_secret_key'] || '';

      if (generatedGUID === '') {
        const options = {
          type: https.RedirectType.SUITELET,
          identifier: currentScript.id,
          id: currentScript.deploymentId,
          parameters: {
            custpage_script_ids: scriptIdsParsed.join(','),
          },
        };
        response.sendRedirect(options);
        return;
      }

      response.writeLine({
        output: `<div><b>Secret GUID: </b>${generatedGUID}</div>`,
      });
      response.writeLine({
        output: `<br/><br/>`,
      });
      response.writeLine({
        output: `<div>
        Copy the value of above Secret GUID and then save it in the configuration record.    
        'On that page you'll also save the S3 Region, Bucket and Access Key.
        </div>`,
      });
      response.writeLine({
        output: `<br/><br/>`,
      });
      response.writeLine({
        output: `<div>
        After copying the GUID click your back button to return to NetSuite.
        </div>`,
      });

    }
  } catch (e) {
    log.debug({title: 'onRequest', details: e});
    response.write({output: JSON.stringify(e)});
  }
};

export default {
  onRequest,
};
