import axios from 'axios';
import Auth from '../Auth';

const { getAuthorizationHeader, ENDPOINT } = Auth;

const requestData = {
  url: ENDPOINT,
  method: 'POST',
};

const requestConfig = context => ({
  url: requestData.url,
  method: requestData.method,
  headers: getAuthorizationHeader(requestData),
  data: context,
});

const postRequest = async (context) => {
  try {
    return await axios(requestConfig(context));
  } catch (error) {
    throw error;
  }
};

export default postRequest;
