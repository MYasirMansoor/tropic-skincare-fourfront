import postRequest from '../Http';
import CONFIG from '../../config';
import {
  listFiles, readCSV, move, getFileName,
} from '../FileSystem';

import parser from './parser';

const { SFTP: { IMPORT_PATH, IMPORT_ARCHIVE_PATH } } = CONFIG;

const syncFlow = 'IMPORT';

const getImportPath = syncType => IMPORT_PATH[syncType];


/**
 * Get Archive path
 * @param {*} fileName
 */
const getArchivePath = (fileName, syncType) => {
  const directory = IMPORT_ARCHIVE_PATH[syncType];
  return path.join(directory, getFileName(fileName));
};


/**
 * Move file to Archive
 * @param {*} filePath
 */
async function archiveFile(filePath, syncType) {
  try {
    await move(filePath, getArchivePath(filePath, syncType));
  } catch (error) {
    throw error;
  }
}


/**
 * Get CSV files and Import to Nestuite
 * @param {recordType, action, processBy, key, chunkLines}
 */
export default async function importCSV({
  recordType,
  action,
} = {}) {
  try {
    // Get path for importing file
    const importPath = getImportPath(recordType);

    // Here we need to list through FileSystem
    const csvFiles = await listFiles(importPath);

    for (const file of csvFiles) {
      const dataToImport = await readCSV(file);
      const filename = file.split('/')[file.split('/').length - 1];

      // Parse JSON of CSV according to record type
      const dataToPost = parser(recordType, dataToImport);
      const data = dataToPost;
      try {
        // Send Post Request to RESTLet
        const response = await postRequest({ action, data, filename });

        if (response.status !== 200 || (response.data && response.data.errorCode)) {
          throw response.error || response.data;
        }
        console.log('Post request sent for ', filename);
      } catch (error) {
        console.error(`${filename}`, { name: error.name || 'error', message: error.message });
        // await postRequest({
        //   action: 'CatchError', message: error.name, messageDetails: error.message, recordType, recordId: null, syncFlow, data, filename,
        // });
      }
    }
  } catch (error) {
    throw error;
  }
}
