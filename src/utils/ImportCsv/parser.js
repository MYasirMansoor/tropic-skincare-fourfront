import SalesOrderParser from '../../modules/SalesOrder/SalesOrderCSVParser';
import CreditMemoParser from '../../modules/CreditMemo/CreditMemoParser';
import WorkOrderIssue from '../../modules/WorkOrderIssue/WorkOrderIssueParser';

const parseMapper = {
  SALES_ORDER: SalesOrderParser,
  ITEM_RECEIPT: '',
  ITEM_FULFILLMENT: '',
  SALES_ORDER_STATUS: '',
  INVENTORY_ADJUSTMENT: '',
  INVENTORY_TRANSFER: '',
  CREDIT_MEMO: CreditMemoParser,
  TRANSFER_ORDER_CONFIRMATION: '',
  WORK_ORDER_ISSUE: WorkOrderIssue,
};


const parseFile = (recordType, data) => parseMapper[recordType](data);

export default parseFile;
