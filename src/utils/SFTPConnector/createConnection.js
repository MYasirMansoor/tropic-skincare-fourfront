import Client from 'ssh2-sftp-client';
import COMMON from '../../config/common';

const { SFTP_CONFIG } = COMMON;

let connectionObj;

const createNewConnection = () => {
    return new Client();
}

const getConnection = async () => {
    if(!connectionObj)
        connectionObj = createNewConnection();
    const temp = await connectionObj.connect(SFTP_CONFIG);
    return connectionObj;
}


export default getConnection;