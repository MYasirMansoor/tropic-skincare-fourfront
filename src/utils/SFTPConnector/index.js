

import _ from 'lodash';
import path from 'path';
import csv from 'csvtojson';
import connection from './createConnection';

let con;


export const readCSV = async (file) => {
  try {
    const data = await con.get(file);

    const csvString = csv({
      noheader: true,
      output: 'csv',
    })
      .fromString(data.toString())
      .then(csvRow => csvRow);
    return csvString;
  } catch (error) {
    throw error;
  }
};

/**
 *
 * @param {*} directoryPath
 */
const readDir = async (directoryPath) => {
  try {
    return await con.list(directoryPath);
  } catch (error) {
    throw error;
  }
};

/**
 * Generate filename from path
 * @param {*} directoryPath
 * @param {*} fileName
 */
const createFileName = (directoryPath, fileName) => {
  try {
    return path.join(directoryPath, fileName.name);
  } catch (error) {
    throw error;
  }
};

/**
 *
 * @param {*} importPath
 */
export const listFiles = async (importPath) => {
  try {
    con = await connection();
    const dir = await readDir(importPath);
    const csvFiles = [];
    dir.map((fileName) => {
      if (fileName.type === '-' && fileName.name.indexOf('S200') !== -1) { csvFiles.push(importPath + fileName.name); }
    });
    return csvFiles;
  } catch (error) {
    throw error;
  }
};

/**
 * Get File name from base path
 * @param {string} fileName
 */
export const getFileName = (fileName) => {
  try {
    return path.basename(fileName);
  } catch (error) {
    throw error;
  }
};
